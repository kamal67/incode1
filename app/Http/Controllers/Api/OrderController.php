<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItems;
use App\Traits\GeneralFunction;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    use GeneralFunction;

    public function __construct(Request $request)
    {


        $this->changeLang(\request('lang'));

    }
    public function getFullInformation()
    {
        $order = Order::getUserOrder(\request()->user()->id);
        return $this->sendResponseWithoutMessage($order);
    }

    public function addItemToOrder(Request $request)
    {
        $rules = [
            'product_id' => 'required|exists:products,id',
        ];
        $vr = $this->validateRequest($request, $rules);
        if ($vr['status'] === false) {
            return $this->sendErrors($vr['errors'], 400);
        }
        $order = Order::getUserOrder(\request()->user()->id);
        OrderItems::addToOrder(["order_id"=>$order->id,"product_id"=>$request->product_id]);
        return $this->sendResponseWithoutMessage($order);
    }


    public function removeItemFromOrderOrder(Request $request)
    {
        $rules = [
            'item_id' => 'required|exists:order_items,id',
        ];
        $vr = $this->validateRequest($request, $rules);
        if ($vr['status'] === false) {
            return $this->sendErrors($vr['errors'], 400);
        }

        OrderItems::delfromOrder($request->item_id );
        $order = Order::getUserOrder(\request()->user()->id);
        return $this->sendResponseWithoutMessage($order);
    }

    public function checkOut()
    {
        $order = Order::getUserOrder(\request()->user()->id);
        if(count($order->items)){
        $res = Order::doneOrder($order);
        return $this->sendResponseWithoutMessage($res);
        }
        else {
            return $this->sendError(__("Your Order Is Empty"),400);
        }


    }


    public function getMyOrders()
    {
        $orders = Order::where(
            [
                ['client_id','=',\request()->user()->id],
                ['status','=',"done"]

            ])->get();
        return $this->sendResponseWithoutMessage($orders);
    }


    public function getOrders($status)
    {
        if(auth()->user()->user_type != "admin")  return $this->sendError(__("forbidden "),403);

        $orders = Order::where(
            [

                ['status','=',$status]

            ])->get();
        return $this->sendResponseWithoutMessage($orders->load('owner'));
    }
}
