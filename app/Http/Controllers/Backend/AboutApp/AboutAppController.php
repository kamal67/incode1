<?php

namespace App\Http\Controllers\Backend\AboutApp;

use App\Http\Controllers\BaseController;
use App\Models\About\AboutApp;
use App\Models\Ads\AdsManagement;
use Illuminate\Http\Request;

class AboutAppController extends BaseController
{

    public function index()
    {
        try {
            $about_en = AboutApp::query()->where('lang', 'en')->first();
            $about_ar = AboutApp::query()->where('lang', 'ar')->first();
            return view('backend.AboutApp.index', compact('about_en', 'about_ar'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    public function create()
    {

    }

    public function store(Request $request)
    {

    }

    public function show($id)
    {

    }

    public function edit()
    {
        try {
            $about_en = AboutApp::query()->where('lang', 'en')->first();
            $about_ar = AboutApp::query()->where('lang', 'ar')->first();
            return \view('backend.AboutApp.edit', compact('about_en', 'about_ar'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    public function update(Request $request)
    {
        try {

            $rules = [
                'information_ar' => 'required',
                'information_en' => 'required',
            ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) return redirect()->back()->withErrors($res_validate['messages']);

            $about_en = AboutApp::query()->where('lang', 'en')->first();
            $about_ar = AboutApp::query()->where('lang', 'ar')->first();

            if ($request->email != null) {
                $about_en->email = $request->email;
                $about_ar->email = $request->email;
            }
            if ($request->hasFile('logo')) {
                $about_en->logo = $this->uploadAppLogo($request->logo);
                $about_ar->logo = $about_en->logo;
            }
            $about_en->information = $request->information_en;
            $about_ar->information = $request->information_ar;

            $about_en->save();
            $about_ar->save();

            return redirect(route('aboutApp.index'))->withStatus(__('custom_messages.general_messages.crud.updated'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    public
    function destroy($id)
    {

    }
}
