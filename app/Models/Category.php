<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'number',
        'media_path',
        'name_ar',
        'name_en',
        'parent_id',
        'is_deleted'
    ];


    protected $hidden = ['created_at', 'updated_at'];

}
