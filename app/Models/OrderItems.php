<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    use HasFactory;
    protected $fillable = [
        "order_id",
        "product_id",
    ];

    public function product() {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function order() {
        return $this->belongsTo(Order::class,'order_id','id');
    }


    public static function addToOrder(array $data){
        self::query()->create($data);
        return true;

    }
    public static function addMultibleToOrder(array $data,$order){

        $details =[];

        foreach ($data["products"] as $item){
            $details[]=[
                'product_id'=>$item,
                'order_id'=>$order->id
            ];
        }
        if(count($details)){
            self::query()->insert($details);
            return true;
        }

        return false;


    }

    public static function delfromOrder($id){
        self::query()->where('id',$id)->delete();
        return true;
    }

}
