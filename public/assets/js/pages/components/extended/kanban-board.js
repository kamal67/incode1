"use strict";

// Class definition

var KTKanbanBoardDemo = function () {

    // Private functions

    // Basic demo
    var demos = function () {



	 
		// kanban 4
		var kanban4 = new jKanban({
			element : '#kanban4',
			gutter  : '0',
			click : function(el){
				alert(el.innerHTML);
			},
			boards  :[
				{
					'id' : '_board1',
					'title'  : 'Board 1',
					'item'  : [
						{
							'title':'My Task Test',
						},
						{
							'title':'Buy Milk',
						}
					]
				},
				{
					'id' : '_board2',
					'title'  : 'Board 2',
					'item'  : [
						{
							'title':'Do Something!',
						},
						{
							'title':'Run?',
						}
					]
				},
				{
					'id' : '_board3',
					'title'  : 'Board 3',
					'item'  : [
						{
							'title':'All right',
						},
						{
							'title':'Ok!',
						}
					]
				}
			]
		});



		var addBoard = document.getElementById('addBoard');
		addBoard.addEventListener('click',function(){
			var boardTitle = $('#kanban-add-board').val();
			var boardId = '_' + $.trim(boardTitle);
			var boardColor = $('#kanban-add-board-color').val();
			var option = '<option value="'+boardId+'">'+boardTitle+'</option>';
			kanban4.addBoards(
				[{
					'id' : boardId,
					'title'  : boardTitle,
					'class': boardColor
				}]
			);
			$('#kanban-select-task').append(option);
			$('#kanban-select-board').append(option);
		});

		var addTask = document.getElementById('addTask');
		addTask.addEventListener('click',function(){
			var target = $('#kanban-select-task').val();
			var title = $('#kanban-add-task').val();
			var taskColor = $('#kanban-add-task-color').val();
			kanban4.addElement(
				target,
				{
					'title': title,
					'class': taskColor
				}
			);
		});

		var removeBoard2 = document.getElementById('removeBoard2');
		removeBoard2.addEventListener('click',function(){
			var target = $('#kanban-select-board').val();
			kanban4.removeBoard(target);
			$('#kanban-select-task option[value="'+target+'"]').remove();
			$('#kanban-select-board option[value="'+target+'"]').remove();
		});
    }

    return {
        // public functions
        init: function() {
            demos();
        }
    };
}();

jQuery(document).ready(function() {
    KTKanbanBoardDemo.init();
});
