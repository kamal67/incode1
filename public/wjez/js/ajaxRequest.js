$(document).ready(function () {
    let select_category = document.querySelector('#select_category');
    let select_sub_cat = document.querySelector('#select_sub_category');
    let cat_id = $('#select_category').val();
    let myUrl = "http://127.0.0.1:8000/get/categories/" + cat_id + "/sub";
    $.ajax({
        url: myUrl,
        type: 'GET',
        success: function (response) {
            $('#select_sub_category').empty();
            $('#select_sub_category').append('<option disabled hidden selected>select</option>');
            for (let i = 0; i < response.data.length; i++) {
                let opt = document.createElement('option');
                opt.value = response.data[i].id;
                if (document.getElementById('local_lang').innerHTML === 'ar') {
                    opt.innerHTML = response.data[i].name_ar;
                } else {
                    opt.innerHTML = response.data[i].name;
                }
                select_sub_cat.appendChild(opt);
            }
            console.log(response);
        },
    });

    select_category.addEventListener('change', function () {
        let cat_id = $('#select_category').val();
        let myUrl = "http://127.0.0.1:8000/get/categories/" + cat_id + "/sub";
        $.ajax({
            url: myUrl,
            type: 'GET',
            success: function (response) {
                $('#select_sub_category').empty();
                $('#select_sub_category').append('<option disabled hidden selected>select</option>');
                for (let i = 0; i < response.data.length; i++) {
                    let opt = document.createElement('option');
                    opt.value = response.data[i].id;
                    if (document.getElementById('local_lang').innerHTML === 'ar') {
                        opt.innerHTML = response.data[i].name_ar;
                    } else {
                        opt.innerHTML = response.data[i].name;
                    }
                    select_sub_cat.appendChild(opt);
                }
                console.log(response);
            },
        });
    });
});
