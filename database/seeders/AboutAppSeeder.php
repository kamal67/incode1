<?php

namespace Database\Seeders;

use App\Models\About\AboutApp;
use Illuminate\Database\Seeder;

class AboutAppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $AboutApp = [
            [   'email' => 'CopyrightⓒWjez',
                'information' => 'As an app advertiser, you want to get your app into the hands of more paying users.
                So, how do you connect with those people? App campaigns streamline the process for you,
                making it easy to promote your apps across Google’s largest properties including Search,
                Google Play, YouTube, Discover on Google Search, and the Google Display Network. Just add a few lines of text,
                a bid, some assets, and the rest is optimized to help your users find you.',
                'app_version'=>'0.0.1',
                'logo'=>'/images/app/logo/1615233871Background Layer@3x.png',
                'lang'=>'en',
                'created_at' => now(), 'updated_at' => now()],

            [   'email' => 'CopyrightⓒWjez',
                'information' => 'بصفتك معلنًا عن التطبيق ، فأنت ترغب في جعل تطبيقك في متناول المزيد من المستخدمين الذين يدفعون.
                لذا ، كيف تتواصل مع هؤلاء الأشخاص؟ تعمل حملات التطبيقات على تبسيط العملية نيابةً عنك ،
                مما يجعل من السهل الترويج لتطبيقاتك عبر أكبر خدمات Google بما في ذلك البحث و Google Play و YouTube و
                Discover on Google Search وشبكة Google الإعلانية. ما عليك سوى إضافة بضعة أسطر من النص وعرض التسعير وبعض الأصول
                وتم تحسين الباقي لمساعدة المستخدمين في العثور عليك.',
                'app_version'=>'0.0.1',
                'logo'=>'/images/app/logo/1615233871Background Layer@3x.png',
                'lang'=>'ar',
                'created_at' => now(), 'updated_at' => now()],
        ];
        AboutApp::query()->insert($AboutApp);
    }
}
