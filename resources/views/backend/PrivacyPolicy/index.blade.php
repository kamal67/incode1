@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.privacy_policy')])
@endsection

@section('content')
    <div class="">
        @include('includes.errors')
    </div>
    <div class="">
        @include('includes.success')
    </div>
    <div class="card-header" style="">
        <div class="card-toolbar row">
            <div class="col-2"></div>
            <div class="col-8 d-flex justify-content-center">
                <a href="{{route('privacyPolicy.edit')}}" class="btn btn-label-brand btn-bold font-weight-bold ">
                    <i class="flaticon2-edit icon-sm"></i>{{__('dashboard.edit')}}
                </a>
            </div>
            <div class="col-2"></div>
        </div>
    </div>
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-tinymce">
                @if(\Illuminate\Support\Facades\Session::get('locale') == 'en')
                    {!! $p_en->description !!}
                @endif
                @if(\Illuminate\Support\Facades\Session::get('locale') == 'ar')
                    {!! $p_ar->description !!}
                @endif
            </div>
        </div>
    </div>

    </div>
@endsection

@section('js')
    <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}" type="text/javascript"></script>
@endsection
