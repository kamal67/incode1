@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.privacy_policy')])
@endsection

@section('content')
    <div class="">
        @include('includes.errors')
    </div>
    <div class="">
        @include('includes.success')
    </div>

    <div class="kt-portlet">
        <h3 class="kt-portlet__body">
            <form action="{{route('privacyPolicy.update')}}" method="post">
                @csrf
                <h3 style="text-align: center;color: #000000;">{{__('dashboard.english')}}</h3>
                <div class="kt-tinymce">
                    <textarea id="privacy_policy_en" name="description_en" class="tox-target">
                        {{$p_en->description}}
                    </textarea>
                </div>
                <h3 style="text-align: center;color: #000000;">{{__('dashboard.arabic')}}</h3>
                <div class="kt-tinymce">
                    <textarea id="privacy_policy_ar" name="description_ar" class="tox-target">
                        {{$p_ar->description}}
                    </textarea>
                </div>

                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2"></div>
                        <div class="col-8 d-flex justify-content-center">
                            <button type="submit"
                                    class="btn btn-label-brand btn-bold font-weight-bold ">
                                <i class="ki ki-check icon-sm"></i>{{__('dashboard.save_change')}}
                            </button>
                            <a href="{{route('privacyPolicy.index')}}"
                               class="btn btn-clean btn-bold">{{__('dashboard.cancel')}}</a>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>

            </form>
        </div>


    </div>
@endsection

@section('js')
    <script src="{{asset('assets/plugins/custom/tinymce/tinymce.bundle.js')}}" type="text/javascript"></script>
    <script !src>
        "use strict";
        $(document).ready(function () {
            tinymce.init({
                selector: 'textarea#privacy_policy_en',
                height: '100%',
                menubar: false,

                plugins: [
                    'advlist autolink lists link image charmap print preview anchor print preview ',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount',

                ],
                toolbar: 'undo redo | formatselect | ' +
                    'bold italic backcolor | alignleft aligncenter ' +
                    'alignright alignjustify | bullist numlist outdent indent | ' +
                    'removeformat ' + 'anchor print preview',
                content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
            });
            tinymce.init({
                selector: 'textarea#privacy_policy_ar',
                height: '100%',
                menubar: false,

                plugins: [
                    'advlist autolink lists link image charmap print preview anchor print preview ',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table paste code help wordcount',

                ],
                toolbar: 'undo redo | formatselect | ' +
                    'bold italic backcolor | alignleft aligncenter ' +
                    'alignright alignjustify | bullist numlist outdent indent | ' +
                    'removeformat ' + 'anchor print preview',
                content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:14px }'
            });
        });
    </script>
@endsection
