
@extends('master_layouts.app')
@section('style')

@endsection
@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('dashboard.orders.management')])
@endsection
@section('content')
    <div class="">
        @include('includes.errors')
    </div>
    <div class="">
        @include('includes.success')
    </div>
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">


            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <form enctype="multipart/form-data" class="form" id="kt_form" method="POST"
                                  action="{{route('orders.store')}}">
                                @csrf
                                <div class="kt-form kt-form--label-right">
                                    <div class="kt-form__body">
                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">

                                                <div class="row">
                                                    <label class="col-xl-3"></label>
                                                    <div class="col-lg-7 col-xl-6">
                                                        <h3 class="kt-section__title kt-section__title-sm">{{__('dashboard.products.info')}}
                                                            :</h3>
                                                    </div>
                                                    <label class="col-xl-2"></label>
                                                </div>
                                                {{-- users --}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.orders.client_name')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select name="user_id"
                                                                class="form-control form-control-solid customSelect" required>
                                                            <option selected disabled hidden
                                                                    value="{{null}}">{{__('dashboard.orders.client_name')}}</option>

                                                            @foreach($users as $us)
                                                                <option value="{{$us->id}}">
                                                                        {{$us->name}}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>




                                                   {{-- products --}}
                                                   <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.orders.products')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select name="products[]"
                                                                class="form-control form-control-solid customSelect  " multiple required>
                                                           

                                                            @foreach($products as $pd)
                                                            <option value="{{$pd->id}}">
                                                                @if(app()->getLocale() =='ar')
                                                                    {{$pd->name_ar}}
                                                                @endif
                                                                @if(app()->getLocale() =='en')
                                                                    {{$pd->name_en}}
                                                                @endif
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>



                                            </div>



                                            </div>








                                            </div>

                                            <div
                                                class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                            <div class="kt-form__actions">
                                                <div class="row d-flex justify-content-center">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <a href="javascript:history.back()"
                                                           class="btn btn-clean btn-bold">
                                                            <i class="ki ki-long-arrow-back icon-sm"></i>{{__('dashboard.back')}}
                                                        </a>
                                                        <button type="submit"
                                                                class="btn btn-label-brand btn-bold font-weight-bold ">
                                                            <i class="ki ki-check icon-sm"></i>{{__('dashboard.products.add')}}
                                                        </button>
                                                    </div>


                                                    <label class="col-xl-3"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

    </div>
@endsection
@section('js')


@endsection

