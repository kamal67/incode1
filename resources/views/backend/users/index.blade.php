@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.users_management')])
@endsection
@section('content')

    <div class="">
        @include('includes.errors')
    </div>
    <div class="">
        @include('includes.success')
    </div>

    <div class="col-12">

        <div class="card-header" style="">
            <div class="card-toolbar row">
                <a href="{{route('dashboard')}}" class="btn btn-light-primary font-weight-bolder mr-2">
                    <i class="flaticon2-back icon-sm"></i>{{__('dashboard.back')}}</a>
                <div class="btn-group">

                    <a href="{{route('users.create',$user_type)}}"
                       class="btn btn-primary font-weight-bolder font-weight-lighter">
                        <i class="flaticon2-add-1 icon-sm"></i>{{__('dashboard.users.add')}}
                    </a>
                </div>
            </div>
        </div>

        <div class="kt-portlet">

            <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                <div class="kt-portlet__body kt-portlet__body--fit ">
                    <div class=" row">
                        <div class="col-md-4 col-sm-12 mt-5 ">
                            <div id="kt_subheader_search mt-2">
                                <form id="kt_subheader_search_form">
                                    <div class="kt-input-icon kt-input-icon--left kt-subheader__search">
                                        <input type="text" class="form-control" placeholder="Search..."
                                               id="generalSearch">
                                        <span class="kt-input-icon__icon kt-input-icon__icon--left">
													<span>
														<svg xmlns="http://www.w3.org/2000/svg"
                                                             xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                                                             height="24px" viewBox="0 0 24 24" version="1.1"
                                                             class="kt-svg-icon">
															<g stroke="none" stroke-width="1" fill="none"
                                                               fill-rule="evenodd">
																<rect x="0" y="0" width="24" height="24"></rect>
																<path
                                                                    d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z"
                                                                    fill="#000000" fill-rule="nonzero"
                                                                    opacity="0.3"></path>
																<path
                                                                    d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z"
                                                                    fill="#000000" fill-rule="nonzero"></path>
															</g>
														</svg>
                                                        <!--<i class="flaticon2-search-1"></i>-->
													</span>
												</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <table class="kt-datatable" id="kt-datatable" width="100%">
                        <thead>
                        <tr>
                            <th data-field="{{__('dashboard.full_name')}}"
                                class="kt-datatable__cell kt-datatable__cell--sort">
                                    <span style="width: 112px; text-align: center">
                                        {{__('dashboard.full_name')}}
                                    </span>
                            </th>

                            <th data-field="{{__('dashboard.type')}}"
                                class="kt-datatable__cell kt-datatable__cell--sort">
                                    <span style="width: 112px; text-align: center">
                                        {{__('dashboard.type')}}
                                    </span>
                            </th>

                            @if($user_type !== 'admin')
                                <th data-field="{{__('dashboard.phone')}}"
                                    class="kt-datatable__cell kt-datatable__cell--sort">
                                    <span style="width: 112px; text-align: center">
                                        {{__('dashboard.phone')}}
                                    </span>
                                </th>
                            @endif

                            <th data-field="{{__('dashboard.account_status.account_status')}}"
                                class="kt-datatable__cell kt-datatable__cell--sort">
                                    <span style="width: 112px; text-align: center">
                                        {{__('dashboard.account_status.account_status')}}
                                    </span>
                            </th>
                            <th data-field="{{__('dashboard.creation_date')}}"
                                class="kt-datatable__cell kt-datatable__cell--sort">
                                    <span style="width: 112px; text-align: center">
                                        {{__('dashboard.creation_date')}}
                                    </span>
                            </th>
                            <th data-field="{{__('dashboard.controls')}}"
                                class="kt-datatable__cell kt-datatable__cell--sort">
                                    <span style="width: 112px; text-align: center">
                                        {{__('dashboard.controls')}}
                                    </span>
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($us as $user)
                            <tr>
                                {{--                                <td style="text-align: center !important;">--}}
                                {{--                                    <div class="kt-user-card-v2 overflow-hidden">--}}
                                {{--                                        <div class="kt-user-card-v2__details">--}}
                                {{--                                            <a class="kt-user-card-v2__name" href="#">{{$user->name}}</a>--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                                {{--                                </td>--}}


                                <td data-field="OrderID" class="kt-datatable__cell" style="width: 175px;">
                                    <span
                                        @if(\Illuminate\Support\Facades\Session::get('locale') =='ar')
                                        style="width: 185px; text-align: right"
                                        @else
                                        style="width: 185px; text-align: left"
                                    @endif
                                    >
                                        <div class="kt-user-card-v2">
                                            <div class="kt-user-card-v2__pic">
                                                @if(!is_null($user->image))
                                                    <img alt="photo" src="{{$user->image}}">
                                                @endif
                                                @if(is_null($user->image))
                                                    <img src="{{asset('assets/media/users/default.jpg')}}" alt="image">
                                                @endif
                                            </div>
                                            <div class="kt-user-card-v2__details">
                                                <span class="kt-user-card-v2__name" style="width: 185px;">
                                                    {{$user->name}}
                                                </span>
                                            </div>
                                        </div>
                                    </span>
                                </td>


                                <td style="text-align: center !important;">
                                    {{$user->user_type}}
                                </td>
                                @if($user_type !== 'admin')
                                    <td style="text-align: center !important;">
                                        {{$user->country_code . $user->phone_number}}
                                    </td>
                                @endif


                                <td style="text-align: center !important;">
                                    <span class=" btn btn-label-brand btn-sm {{$user->account_status == 'active'? 'btn-label-success ':'btn-label-danger ' }}">
                                        {{$user->account_status}}
                                    </span>

                                </td>

                                <td style="text-align: center !important;">
                                    {{ $user->created_at->format('Y-m-d') }}
                                </td>

                                <td style="text-align: center !important;">
                                    <div class="dropdown ">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           data-toggle="dropdown">
                                            <i class="flaticon-more-1"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <ul class="kt-nav">


                                                    <li class="kt-nav__item">
                                                        <a id="delete_user" class=" kt-nav__link"
                                                           style="cursor: pointer "
                                                                                                                  href="{{route('users.delete',$user->id)}}"
                                                           title="   ">
                                                            <i class=" kt-nav__link-icon flaticon-delete"></i>
                                                            <span
                                                                class="kt-nav__link-text">{{__('dashboard.delete')}}</span>
                                                        </a>
                                                    </li>


                                            </ul>
                                        </div>
                                    </div>
                                </td>

                            </tr>

                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{asset('wjez/js/user_table.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#delete_user').onclick(function (e) {
                e.preventDefault();
                alert('Hello');
            });
        });
    </script>
@endsection

