@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.about_app')])
@endsection
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="">
            @include('includes.errors')
        </div>
        <div class="">
            @include('includes.success')
        </div>
        <div class="kt-portlet__body">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                <form enctype="multipart/form-data" class="form" id="kt_form" method="POST"
                      action="{{route('aboutApp.update')}}">
                    @csrf
                    <div class="kt-form kt-form--label-right">
                        <div class="kt-form__body">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">


                                    <div class="form-group row">
                                        <div class="col-3"></div>
                                        <div class="col-6  d-flex justify-content-center">
                                            <div class="kt-avatar kt-avatar--outline "
                                                 id="kt_user_edit_avatar">
                                                <div class="kt-avatar__holder"
                                                     @if(!is_null($about_en))style="background-image: url('{{$about_ar->logo}}');width: 250px;"@endif
                                                ></div>

                                                <label class="kt-avatar__upload" data-toggle="kt-tooltip"
                                                       title="" data-original-title="Change image">
                                                    <i class="fa fa-pen"></i>
                                                    <input type="file" name="logo"
                                                           accept=".png, .jpg, .jpeg">
                                                </label>
                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip"
                                                      title="" data-original-title="Cancel image">
																				<i class="fa fa-times"></i>
																			</span>
                                            </div>
                                        </div>
                                        <label class="col-3"></label>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-3"></div>
                                        <div class="col-6 d-flex justify-content-center">
                                            <h5><input class="form-control" type="text" name="email" style="text-align: center"
                                                       value="@if(!is_null($about_en)){{$about_en->email}}@endif"></h5>
                                        </div>
                                        <div class="col-3"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-3"></div>
                                        <div class="col-6 d-flex justify-content-center">
                                            <h5 style="color: #62ceeb">
                                                Version @if(!is_null($about_en)){{$about_en->app_version}}@endif</h5>
                                        </div>
                                        <div class="col-3"></div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h5>{{__('dashboard.english')}}</h5>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <textarea class="form-control" name="information_en"
                                                      style="height: 220px;width: 80%;text-align: left;">@if(!is_null($about_en)){{$about_en->information}}@endif</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <h5>{{__('dashboard.arabic')}}</h5>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-12 d-flex justify-content-center">
                                            <textarea class="form-control " name="information_ar"
                                                      style="height: 220px;width: 80%;text-align: right;">@if(!is_null($about_ar)){{$about_ar->information}}@endif</textarea>
                                        </div>
                                    </div>

                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-8 d-flex justify-content-center">
                                                <button type="submit"
                                                        class="btn btn-label-brand btn-bold font-weight-bold ">
                                                    <i class="ki ki-check icon-sm"></i>{{__('dashboard.save_change')}}
                                                </button>
                                                <a href="{{route('aboutApp.index')}}"
                                                   class="btn btn-clean btn-bold">{{__('dashboard.cancel')}}</a>
                                            </div>
                                            <div class="col-2"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('wjez/js/user_table.js')}}"></script>
    <script src="{{asset('assets/js/pages/custom/user/edit-user.js')}}"></script>
@endsection

