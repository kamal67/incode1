<?php
return [

    'general_messages' => [
        'created_success' => 'Created successfully.',
        'reset_password' => 'Password changed successfully.',
        'old_password_incorrect' => 'The old password is incorrect .',
        'image_changed' => ' The profile picture has been changed .',
        'item_not_found' => ' item not found .',
        'user_not_found' => ' user not found .',
        'request_not_found' => ' Request not found .',
        'bad_user_type' => 'Please select user type customer or provider',
        'location_update'=>'Location has been updated',
        'information_updated'=>'information has been updated.',
        'change_request_status'=>'The status of the request has changed',
        'your_phone_is_private' => 'We\'ll never share your phone number with anyone else.',
        'crud'=> [
            'created'=>'created successfully.',
            'updated'=>'updated successfully.',
            'deleted'=>'deleted successfully.',
        ]
    ]
];
