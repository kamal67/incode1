<?php

return [
    'dashboard' => 'لوحة التحكم',
    'users' => 'المستخدمين',

    'admins' => 'المدراء',

    'category' => 'الفئات',
    'brands' => 'الماركات',

    'main_category' => 'الفئات ',

    'about_app' => 'حول التطبيق',
    'privacy_policy' => 'سياسة الخصوصية',
    'all_ads' => 'كل الإعلانات',
    'create_ad' => 'انشاء اعلان',

    'products_all' => 'جميع المنتجات',

    'products' => 'المنتجات ',
];
