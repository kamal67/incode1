<?php
return [

    'general_messages' => [
        'created_success' => 'تم الانشاء بنجاح',
        'reset_password' => 'تم تغيير كلمة المرور بنجاح.',
        'old_password_incorrect' => 'كلمة المرور القديمة غير صحيحة.',
        'image_changed' => ' تم تغيير الصورة الشخصية .',
        'item_not_found' => ' العنصر غير موجود .',
        'bad_user_type' => 'الرجاء اختيار نوع المستخدم customer أو provider',
        'location_updated' => 'تم تحديث الموقع.',
        'information_updated' => 'تم تحديث المعلومات.',
        'change_request_status' => 'تم تغيير حالة الطلب.',
        'user_not_found' => 'المستخدم غير موجود',
        'request_not_found' => 'الطلب غير موجود',
        'your_phone_is_private' => 'لن نشارك رقم هاتفك مع أي شخص آخر.',
        'crud'=> [
            'created'=>'تم الانشاء بنجاح . ',
            'updated'=>'تم التحديث بنجاح . ',
            'deleted'=>'تم الحذف بنجاح . ',
        ]
    ]
];
