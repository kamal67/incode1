<?php

return [
    'users_management' => 'إدارة المستخدمين',
    'request_management' => 'إدارة الطلبات',
    'category_management' => 'إدارة الأقسام',
    'brands_management' => 'إدارة الماركات',
    'sliders_management' => 'إدارة السلايدر',
    'wrap_management' => ' إدارة التغليف',
    'products_management' => 'إدارة المنتجات',
    'occasions_management' => 'إدارة المناسبات',
    'relations_management' => 'إدارة العلاقات',
    'ads_management' => 'إدارة الإعلانات',
    'about_app' => 'حول التطبيق',
    'privacy_policy' => 'سياسة الخصوصية',
    'login' => 'تسجيل الدخول',
    'wjez' => 'وجيز',
    'home_page' => 'الصفحة الرئيسية',

    'song_management' => 'إدارة الاغنية',
];
